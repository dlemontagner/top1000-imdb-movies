# top1000-imdb-movies

This project is an illustration for this article :  [API for returning CSV's data with Quarkus in native mode](https://david.le-montagner.fr/posts/api-restitution-csv-quarkus-native/). Sorry, it's only available in French for the moment. 

---

Ce projet est une illustration pour cet article : [API de restitution d’un CSV avec Quarkus en mode natif](https://david.le-montagner.fr/posts/api-restitution-csv-quarkus-native/)
