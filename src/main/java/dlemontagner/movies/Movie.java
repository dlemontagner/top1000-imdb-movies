package dlemontagner.movies;

public record Movie(
        String Poster_Link,
        String Series_Title,
        String Released_Year,
        String Certificate,
        String Runtime,
        String Genre,
        float IMDB_Rating,
        String Overview,
        int Meta_score,
        String Director,
        String Star1,
        String Star2,
        String Star3,
        String Star4,
        long No_of_Votes,
        String Gross
) {
}
