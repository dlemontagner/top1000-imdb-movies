package dlemontagner.movies;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MoviesCSVIngestor {
    private final String moviesCSVSrc;
    private final MoviesRepository moviesRepository;

    public MoviesCSVIngestor(@ConfigProperty(name = "movies.csv.src") String moviesCSVSrc, MoviesRepository moviesRepository) {
        this.moviesCSVSrc = moviesCSVSrc;
        this.moviesRepository = moviesRepository;
    }

    public void ingest(@Observes StartupEvent event) throws IOException {
        Log.infof("Ingesting source: %s", moviesCSVSrc);
        try (MappingIterator<Movie> it = this.fetchData()) {
            List<Movie> movies = it.readAll();
            moviesRepository.setMovies(movies);
            Log.infof("Ingested %d movies", movies.size());
        }
    }

    public MappingIterator<Movie> fetchData() throws IOException {
        ObjectReader objectReader = new CsvMapper()
                .readerFor(Movie.class)
                .with(CsvSchema.emptySchema().withHeader());

        InputStream csvStream = getClass().getClassLoader().getResourceAsStream(moviesCSVSrc);
        return objectReader.readValues(csvStream);
    }
}
