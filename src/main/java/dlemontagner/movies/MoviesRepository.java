package dlemontagner.movies;

import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Random;

@ApplicationScoped
public class MoviesRepository {
    private List<Movie> movies;
    private Random rand;

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        this.rand = new Random();
    }

    public List<Movie> getAll(){
        return movies;
    }

    public Movie getRandom(){
        return movies.get(rand.nextInt(movies.size()));
    }
}
