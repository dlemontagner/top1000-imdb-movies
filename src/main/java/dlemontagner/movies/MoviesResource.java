package dlemontagner.movies;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

@Path("/")
public class MoviesResource {
    public final MoviesRepository moviesRepository;

    public MoviesResource(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Movie> allMovies(){
        return moviesRepository.getAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/random")
    public Movie randomMovie(){
        return moviesRepository.getRandom();
    }
}
